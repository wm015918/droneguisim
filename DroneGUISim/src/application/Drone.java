package application;

import java.util.ArrayList;

import javafx.scene.image.Image;

public class Drone {
	private double droneX, droneY, droneWidth, droneHeight,  droneAngle, droneSpeed;
		// position and size of ball + angle traveling and speed
	
	ArrayList<Drone> droneList = new ArrayList<Drone>();
	
	public Drone() {
		droneX = 100;
		droneY = 180;
		droneWidth = 20;
		droneHeight = 20;
		droneAngle = 45;
		droneSpeed = 1;
	}
	/**
	 * function to check where ball is, and adjust angle if is hitting side of canvas
	 * @param xSize		max x size of arena
	 * @param ySize
	 */
	public void canMoveHere(MyArena mc) {
		
		if (droneX+droneWidth > mc.getXCanvasSize() || droneX <= 1 ) droneAngle = 180 - droneAngle;
			// if ball hit (tried to go through) left or right walls, set mirror angle, being 180-angle
		if (droneY+droneHeight > mc.getYCanvasSize()|| droneY <= 1) droneAngle = -droneAngle;
			// if ball hit (tried to go through) top or bottom walls, set mirror angle, being -angle
	}
	/**
	 * move the ball according to speed and angle
	 */
	public void adjustAngle() {
		double radAngle = droneAngle*Math.PI/180;	// put angle in radians
		droneX += droneSpeed * Math.cos(radAngle);		// new X position
		droneY += droneSpeed * Math.sin(radAngle);		// new Y position
	}
	/**
	 * update the world -
	 * @param mc	canvas in which drawn
	 */
	public void updateWorld(MyArena mc) {
		canMoveHere(mc);			// check if about to hit side of wall, adjust angle if so
		adjustAngle();			// calculate new position
		mc.showInt(droneX, droneY, 0);
	}
	/**
	 * draw the ball into the canvas mc
	 * @param mc
	 */
	public void drawWorld(MyArena mc) {
		mc.clearCanvas();
		mc.showDrone(droneX, droneY, droneWidth, droneHeight, 'r');	
		
		//mc.showDrone(200, 200, droneWidth, droneHeight, 'b');// call interface's routine
		
		
	}
	/**
	 * set ball to position x,y
	 * @param x
	 * @param y
	 */
	public void setXY(double x, double y) {
		droneX = x;
		droneY = y;
	}
	/**
	 * return string describing ball and its position
	 */
	public String toString() {
		return "Drone at " + Math.round(droneX) + ", " + Math.round(droneY);
	}

}
